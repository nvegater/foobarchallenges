package org.tasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SolutionTest {

    @Test
    void testSolution1() {
        int expected1 = 1;
        int actual1 = Solution.solution("1211", 10);
        /*

        from the example:

        given minion ID
            n = 1211,
            k = 4,
            b = 10,
            then
            x = 2111,
            y = 1112
            z = 2111 - 1112 = 0999

    MinionID: 0999 -> CycleLength: 0
    Check if the ID Has been seen
    If not, continue the loop
    .
    .
    MinionID: 8991 -> CycleLength: 1
    MinionID: 8082 -> CycleLength: 2
    MinionID: 8532 -> CycleLength: 3
    MinionID: 6174 -> CycleLength: 4 <-------- Here we have 6174
    In the next loop it finds the MinionID in the map
    MinionID: 6174 -> CycleLength: 5 <-------- Here we find it again 6174
    Check if the ID Has been seen?: true

    Returns the
    Difference of the Cycle where the ID was found
    ( 5 )
    Minus the Last Cycle of the appearance of the seen ID
    ( 4 )

    Therefore 1.

        * */
        assertEquals(expected1, actual1);
    }

    @Test
    void testSolution2() {
        int expected1 = 3;
        int actual1 = Solution.solution("210022", 3);

        /*

For example, starting with n = 210022, k = 6, b = 3,
the algorithm will reach the cycle of values
[210111, 122221, 102212]

It will find the value "210111" again
and it will stay in this cycle no matter how many times it continues iterating.

MinionID: 220101 -> CycleLength: 0
Check if the ID Has been seen?: false
MinionID: 212201 -> CycleLength: 1
Check if the ID Has been seen?: false
MinionID: 210111 -> CycleLength: 2 <-------- Here we have 210111
Check if the ID Has been seen?: false
MinionID: 122221 -> CycleLength: 3
Check if the ID Has been seen?: false
MinionID: 102212 -> CycleLength: 4
Check if the ID Has been seen?: false
MinionID: 210111 -> CycleLength: 5 <-------- Here we find it again 210111
Check if the ID Has been seen?: true

 Returns the
    Difference of the Cycle where the ID was found
    ( 5 )
    Minus the Last Cycle of the appearance of the seen ID
    ( 2 )

    Therefore 3.

        * */
        assertEquals(expected1, actual1);
    }

    @Test
    public void testBinarySubtraction() {
        String result = Solution.subtract("1010", "101", 2);
        assertEquals("101", result);
    }

    @Test
    public void testDecimalSubtraction() {
        String result = Solution.subtract("1234", "567", 10);
        assertEquals("667", result);
    }

    @Test
    public void testMixedBaseSubtraction() {
        String result = Solution.subtract("1010", "21", 3);
        assertEquals("212", result);
    }

    @Test
    public void testMixedBaseSubtractionYoutvideo() {
        String result = Solution.subtract("2110", "1012", 3);
        assertEquals("1021", result);
    }
    @Test
    public void testSortStringAscending() {
        String input = "528319";
        String expected = "123589";
        String actual = Solution.sortDigitsOfMinionId(input, true);
        assertEquals(expected, actual);
    }

    @Test
    public void testSortStringDescending1() {
        String input = "528319";
        String expected = "985321";
        String actual = Solution.sortDigitsOfMinionId(input, false);
        assertEquals(expected, actual);
    }

    @Test
    public void testSortStringDescendingExample2Asc() {
        String input = "210022";
        String expected = "001222";
        String actual = Solution.sortDigitsOfMinionId(input, true);
        assertEquals(expected, actual);
    }

    @Test
    public void testSortStringDescendingExample2Desc() {
        String input = "210022";
        String expected = "222100";
        String actual = Solution.sortDigitsOfMinionId(input, false);
        assertEquals(expected, actual);
    }

    @Test
    public void testSortStringDescending2() {
        String input = "0999";
        String expected = "9990";
        String actual = Solution.sortDigitsOfMinionId(input, false);
        assertEquals(expected, actual);
    }

    @Test
    public void testSortStringEmptyInput() {
        String input = "";
        String expected = "";
        String actual = Solution.sortDigitsOfMinionId(input, true);
        assertEquals(expected, actual);
    }

    @Test
    public void testSortStringSingleNumber() {
        String input = "5";
        String expected = "5";
        String actual = Solution.sortDigitsOfMinionId(input, true);
        assertEquals(expected, actual);
    }

    @Test
    public void testSortStringAlreadySorted() {
        String input = "123456";
        String expected = "123456";
        String actual = Solution.sortDigitsOfMinionId(input, true);
        assertEquals(expected, actual);
    }
    @Test
    public void testAddLeadingZerosNoPadding() {
        int n = 42;
        int k = 2;
        String expected = "42";
        String actual = Solution.addLeadingZeros(n, k);
        assertEquals(expected, actual);
    }

    @Test
    public void testAddLeadingZerosPadding() {
        int n = 42;
        int k = 5;
        String expected = "00042";
        String actual = Solution.addLeadingZeros(n, k);
        assertEquals(expected, actual);
    }

    @Test
    public void testAddLeadingZerosEqualLength() {
        int n = 12345;
        int k = 5;
        String expected = "12345";
        String actual = Solution.addLeadingZeros(n, k);
        assertEquals(expected, actual);
    }

    @Test
    public void testAddLeadingZerosZero() {
        int n = 0;
        int k = 3;
        String expected = "000";
        String actual = Solution.addLeadingZeros(n, k);
        assertEquals(expected, actual);
    }

}