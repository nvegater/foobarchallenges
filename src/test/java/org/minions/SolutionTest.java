package org.minions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SolutionTest {

    @Test
    void solution() {

        assertEquals("23571", Solution.solution(0));
        assertEquals("71113", Solution.solution(3));
        assertEquals("92329", Solution.solution(11));

        assertEquals("23293", Solution.solution(12));
        assertEquals("32931", Solution.solution(13));
        assertEquals("20023", Solution.solution(9894));
        assertEquals("00232", Solution.solution(9895));
        assertEquals("02192", Solution.solution(10000));

    }
}