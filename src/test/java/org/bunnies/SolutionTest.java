package org.bunnies;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SolutionTest {

    @Test
    void solution() {
        assertEquals("7", Solution.solution(1,4));

        assertEquals("4", Solution.solution(1,3));
        assertEquals("8", Solution.solution(2,3));

        assertEquals("2", Solution.solution(1,2));
        assertEquals("5", Solution.solution(2,2));
        assertEquals("9", Solution.solution(3,2));

        assertEquals("1", Solution.solution(1,1));
        assertEquals("3", Solution.solution(2,1));
        assertEquals("6", Solution.solution(3,1));
        assertEquals("10", Solution.solution(4,1));

    }
}