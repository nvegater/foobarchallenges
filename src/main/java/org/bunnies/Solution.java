package org.bunnies;

public class Solution {

    public static String solution(long x, long y) {

        // calculate the number of elements in the diagonal that contains
        long n = x + y - 1;
        // Calculate the number of elements
        // in all the diagonals
        // that come before the diagonal that contains (x, y).
        // This is equal to the sum of the first n-1 integers,
        // where n is the number of elements in the diagonal.
        // This is equal to n * (n - 1) / 2.
        long res = n * (n - 1) / 2;
        // Finally, we add the index of the element (x)
        // to the value of the previous diagonal elements
        return String.valueOf(res + x);
    }

}
