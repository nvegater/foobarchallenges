# Bunny Worker Locations

## Intro
Keeping track of Commander Lambda's many bunny workers is starting to get tricky. 
You've been tasked with writing a program to: 
* match bunny worker IDs --> to cell locations (x,y)


The LAMBCHOP doomsday device takes up much of the interior of Commander Lambda's space station
and as a result the work areas have an unusual layout  -> Big fucking Lambchop doomsday device makes things weird

They are stacked in a triangular shape, 
and the bunny workers are given numerical IDs starting from the corner, 

as follows:

| 7
| 4 8
| 2 5 9
| 1 3 6 10

Each cell can be represented as points (x, y)
*  x being the distance from the vertical wall 
*  y being the height from the ground.

Examples, a bunny worker...
* at (1, 1) has ID 1 
* at (3, 2) has ID 9
* at (2,3) has ID 8.
* This pattern of numbering continues indefinitely (Commander Lambda has been adding a LOT of workers).

Write a function solution(x, y) which returns the worker ID of the bunny at location (x, y). 
Each value of x and y will be at least 1 and no greater than 100,000. 
Since the worker ID can be very large, return your solution as a string representation of the number.

## ChatGpt prompt
Write a Java function in this form:
public class Solution {

    public static String solution(int x, int y) {

      
        return "";
    }

}

That passes following unit tests:
assertEquals("7", Solution.solution(1,4));

        assertEquals("4", Solution.solution(1,3));
        assertEquals("8", Solution.solution(2,3));

        assertEquals("2", Solution.solution(1,2));
        assertEquals("5", Solution.solution(2,2));
        assertEquals("9", Solution.solution(3,2));

        assertEquals("1", Solution.solution(1,1));
        assertEquals("3", Solution.solution(2,1));
        assertEquals("6", Solution.solution(3,1));
        assertEquals("10", Solution.solution(4,1));

It does not work, the test " assertEquals("7", Solution.solution(1,4));" Actually equals to 10. Correct that

## referral Code

https://foobar.withgoogle.com/?eid=ID7AI


Languages
=========

To provide a Java solution, edit Solution.java

Test cases
==========
Your code should pass the following test cases.
Note that it may also be run against hidden test cases not shown here.

-- Java cases --
Input:
Solution.solution(3, 2)
Output:
9

Input:
Solution.solution(5, 10)
Output:
96