package org.minions;

public class Solution {

    public static String solution(int n) {

        // Add the rest of the primes to this var
        StringBuilder givenString = new StringBuilder("2357111317192329");

        // if there are only 23571 (5 digits I can supply 1 minion)
        // if there are only 235711 (6 digits I can supply 1 minion)
        // only if there are only 2357111317 (10 digits I can supply 2 minions)
        // to supply 3 minions I need 15, 4 I need 20 and so on.

        // 5 * Number of minions is the number of digits I need
        // 10000 * 5 = 50000
        int digitsPerId = 5;


        // Each prime number I add to the String, adds a digit
        // I dont need to add any more primes after I have 50,000 digits

        // I can start with 31 because thats the next prime number
        int candidateDigits = 31;

        while (givenString.length() < 50000) {

            boolean isOdd = candidateDigits % 2 != 0;

            if (isOdd){
                // check odd numbers to see if theyre prime
                if (isPrime(candidateDigits)){
                    String stringPrime = Integer.toString(candidateDigits);
                    givenString.append(stringPrime);
                }
            }

            candidateDigits++;

        }


        return givenString.substring(n, n + digitsPerId);
    }

    public static boolean isPrime(int n) {
        //  Since 1 is not a prime number and there are no prime numbers less than 2,
        //  any number less than or equal to 1 is not prime,
        //  so the function immediately returns false in this case.



        // A better mental model is we want to prove that n is a non-Prime (composite)
        // by elimination
        // Eliminate 1 (because 1 or anything smaller is not prime)
        // Because we excluded 1 already
        // if n is a composite its made of two factors n = a * b

        // If Both factors (a&b) are equal each factor is the sqrt of n
        // and that is fine...
        // But not useful to find composites.
        // So we assume both factors are different.

        // To fulfill n = a * b
        // both factors CANT be bigger than sqrt of N
        // both factors CANT be smaller than sqrt of N either

        // If both cant be bigger or smaller, this must be true to fulfill n = a*b :
        //      one is SMALLER or is the sqrt( n )
        //      and
        //      one is BIGGER or is the sqrt( n )
        //
        // If both factors were greater than the square root of n,
        // then a*b would be greater than n.
        // There are infinite factors bigger than the sqr root of n
        // and that is fine....
        // But we cannot iterate through infinity
        // Therefore we only need to check factors smaller than the sqrt of n.

        // n = 1 (or less) kills the chance of prime
        if (n <= 1) {
            return false;
        }
        // for n = 2, initialFactor is bigger than sqrt(n)
        // since we start with initialFactor=2: 2<=sqrt(2) is false
        // so skip the loop and return true, is prime
        // for n=3 the same
        int initialFactor = 2;
        for (int factor = initialFactor; factor <= Math.sqrt(n); factor++) {
            // until n=4 we enter the loop
            int residualOfDivision = n % factor;
            if (residualOfDivision == 0) {
                // for n=4, 2 is a factor.
                return false;
            }
        }
        return true;
    }
}
