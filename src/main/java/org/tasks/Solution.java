package org.tasks;

import java.util.HashMap;
import java.util.stream.IntStream;

public class Solution {
    public static int solution(String n, int b) {

        HashMap<String, Integer> seen = new HashMap<>();
        int cycleLength = 0;

        String nextN = n;

        while (!seen.containsKey(nextN)) {

            seen.put(nextN, cycleLength);

            String x = sortDigitsOfMinionId(nextN, false);

            String y = sortDigitsOfMinionId(nextN, true);

            String subtraction = subtract(x, y, b);

            int z = Integer.parseInt(subtraction);

            nextN = addLeadingZeros(z, n.length());

            cycleLength++;
        }

        return cycleLength - seen.get(nextN);
    }

    public static String subtract(String a, String b, int base) {

        int x = Integer.parseInt(a,base);
        int y = Integer.parseInt(b,base);

        return Integer.toString(x-y,base);
    }

    public static String sortDigitsOfMinionId(String str, boolean ascending) {
        int[] arr = str.chars().map(Character::getNumericValue).sorted().toArray();
        if (!ascending) {
            int[] finalArr = arr;
            arr = IntStream.range(0, arr.length).map(i -> finalArr[finalArr.length - 1 - i]).toArray();
        }
        StringBuilder result = new StringBuilder();
        for (int j : arr) {
            result.append(j);
        }
        return result.toString();
    }

    public static String addLeadingZeros(int n, int k) {
        String s = String.valueOf(n);
        if (s.length() >= k) {
            return s;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < k - s.length(); i++) {
            sb.append("0");
        }
        sb.append(s);
        return sb.toString();
    }

}
